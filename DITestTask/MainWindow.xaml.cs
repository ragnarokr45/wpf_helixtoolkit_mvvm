﻿using DITestTask.ViewModel;
using HelixToolkit.Wpf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DITestTask
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public List<Task> Tasks;

        public MainWindow()
        {
            InitializeComponent();

            DataContext = new TestTaskViewModel();
        }

        private Grid activeGrid;
        /// <summary>
        /// Определяет Grid на котором вызвано событие и окрашивает его в другой цвет.
        /// </summary>
        private void TaskGrid_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            Grid taskGrid = sender as Grid;
            if (activeGrid != taskGrid && activeGrid != null) activeGrid.Background = taskGrid.Background;
            activeGrid = taskGrid;
            taskGrid.Background = Brushes.LightSlateGray;
        }
    }
}

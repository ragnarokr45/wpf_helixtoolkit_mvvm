﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Media3D;
using System.Windows.Threading;

namespace DITestTask.ViewModel
{
    class TestTaskViewModel:BaseViewModel
    {
        private TaskModel selectedTask;
        private Model3DGroup model3D;
        private Rect3D bBox;

        public Model3DGroup Model3D
        {
            get { return model3D; }
            set
            {
                model3D = value;
                OnPropertyChanged();
            }
        }

        public Rect3D BBox
        {
            get { return bBox; }
            set
            {
                bBox = value;
                OnPropertyChanged();
            }
        }

        ObjDialogService objDialog = new ObjDialogService();

     

        private RelayCommand loadModel;
        public  RelayCommand LoadModel
        {
            get
            {
                return loadModel ??
                    (loadModel = new RelayCommand(async obj =>
                    {
                       objDialog.OpenFileDialog();
                       Model3D = await await Task.Factory.StartNew<Task<Model3DGroup>>(
                                                                () => objDialog.LoadModel(),
                                                                TaskCreationOptions.LongRunning);
                       BBox = objDialog.BBox;

                    }));
            }
        }

        private RelayCommand clearModel;
        public RelayCommand ClearModel
        {
            get
            {
                return clearModel ??
                    (clearModel = new RelayCommand(obj =>
                    {
                        objDialog.BBox = default(Rect3D);
                        objDialog.Model = null;
                        Model3D = null;
                        BBox = default(Rect3D);
                    }));
            }
        }
        
        public ObservableCollection<TaskModel> Tasks { get; set; }
        public TaskModel SelectedTask
        {
            get { return selectedTask; }
            set
            {
                selectedTask = value;
                OnPropertyChanged();
            }
        }

        public TestTaskViewModel()
        {
            
            Tasks = new ObservableCollection<TaskModel>
            {
                new TaskModel{Id = 1, Title = "Task 3"},
                new TaskModel{Id = 2, Title = "Task 4"},
                new TaskModel{Id = 3, Title = "Task 5"}
            };
        }
       


    }
}

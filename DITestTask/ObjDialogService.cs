﻿using HelixToolkit.Wpf;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows.Media.Media3D;

namespace DITestTask
{
    class  ObjDialogService
    {
        public string FilePath { get; set; }
        public Model3DGroup Model { get; set; }
        public Rect3D BBox { get; set; }


        public  void OpenFileDialog()
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "obj files(*.obj)|*.obj";
            bool? result = openFileDialog.ShowDialog();
            if (result == true)
            {
             FilePath = openFileDialog.FileName;
               
            }
           
        }

        public async Task<Model3DGroup> LoadModel()
        {

             ModelImporter import = new ModelImporter();

            Model3DGroup model = await Task.Run(() => import.Load(FilePath));
            
            Model = model;
            Model.Freeze();
            BBox = Model.Bounds;
            return Model;
             
        }
        
    }
}
